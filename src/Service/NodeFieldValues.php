<?php

namespace Drupal\node_field_values\Service;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\RevisionableStorageInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\node\Entity\Node;

/**
 * Class NodeFieldValues.
 *
 * Gets the field values out of a node.
 *
 * @package Drupal\node_values\Service
 */
class NodeFieldValues {

  /**
   * Entity type manager from core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Revisionable storage from core.
   *
   * @var \Drupal\Core\Entity\RevisionableStorageInterface
   */
  private RevisionableStorageInterface $revisionableStorage;

  /**
   * Default constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Function to get the values of the node.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node object.
   *
   * @return array
   *   The array of values for the node.
   */
  public function getValues(Node $node): array {

    return $this->entityToArrayWithReferences($node);
  }

  /**
   * Converts the given entity to an array with referenced entities loaded.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The target entity.
   * @param int $depth
   *   Internal. Track the recursion.
   * @param array $array_path
   *   Internal. Track where we first say this entity.
   *
   * @return mixed[]
   *   An array of field names and deep values.
   */
  protected function entityToArrayWithReferences(
    EntityInterface $entity,
    int $depth = 0,
    array $array_path = [],
  ) {

    // Ensure that we process at least these fields, so that
    // there are values in at least in some fields.
    $fields_to_process = [
      'nid',
      'vid',
      'mid',
      'fid',
      'name',
      'tid',
      'layout_builder__layout',
    ];

    // This will ensure that we do not duplicate traversing a field.
    $seen = &drupal_static(__FUNCTION__);
    $seen_key = $entity->getEntityTypeId() . '-' . $entity->id();
    if (!isset($seen[$seen_key])) {
      $seen[$seen_key] = $array_path;
    }

    // Convert the entity to an array.
    $array = $entity->toArray();

    // Prevent out of memory and too deep traversing.
    if ($depth > 20) {
      return $array;
    }

    // If this is already entity just return the array.
    if (!$entity instanceof FieldableEntityInterface) {
      return $array;
    }

    // Step through each of the fields and get the values.
    foreach ($array as $field => &$value) {

      // Only get the fields that we want, which are the fields
      // to process from above and actual fields.
      if (
        str_starts_with($field, 'field_') ||
        in_array($field, $fields_to_process)
      ) {

        // If there is a value, continue to process.
        if (is_array($value)) {

          // Get the field definition and target type from the field.
          $fieldDefinition = $entity->getFieldDefinition($field);
          $target_type = $fieldDefinition->getSetting('target_type');

          // If there is no target type, skip, so it will not
          // cause errors.
          if (!$target_type) {
            continue;
          }

          // Try and load the target type.
          try {
            $storage = $this->entityTypeManager->getStorage($target_type);
          }
          catch (InvalidPluginDefinitionException | PluginNotFoundException) {
            continue;
          }

          // Step through each of the values, and get its values recursively.
          foreach ($value as $delta => &$item) {

            // If this is an array of values, get the referenced entity
            // and process them.
            if (is_array($item)) {

              // Set the referenced entity to null, so that we will not
              // process if we do not get anymore values.
              $referenced_entity = NULL;

              // If there is a target id or a target revision id, load it.
              if (isset($item['target_id'])) {
                $referenced_entity = $storage->load($item['target_id']);
              }
              elseif (isset($item['target_revision_id'])) {
                $referenced_entity = $this->revisionableStorage->loadRevision($item['target_revision_id']);
              }

              // Get the language code of the entity.
              $langcode = $entity->language()->getId();

              // If this is translatable, get the translation of the entity
              // as well.
              if (
                $referenced_entity instanceof TranslatableInterface &&
                $referenced_entity->hasTranslation($langcode)
              ) {
                $referenced_entity = $referenced_entity->getTranslation($langcode);
              }

              // If there is no referenced entity, skip.
              if (empty($referenced_entity)) {
                continue;
              }

              // Update the recursion info.
              $seen_id = $referenced_entity->getEntityTypeId() . '-' . $referenced_entity->id();
              if (isset($seen[$seen_id])) {
                $item['message'] = 'Recursion detected.';
                $item['array_path'] = implode('.', $seen[$seen_id]);
                continue;
              }

              // Get the item recursively.
              $item = $this->entityToArrayWithReferences(
                $referenced_entity,
                $depth++,
                array_merge(
                  $array_path,
                  [$field, $delta, 'entity']
                )
              );
            }
          }
        }
      }
      else {
        unset($array[$field]);
      }
    }

    return $array;
  }

}
