# Node Field Values
This module provides the ability to get all the field values of a node programatically.


# Installation
* If you [manage your site dependencies via Composer](https://www.drupal.org/docs/develop/using-composer/using-composer-to-manage-drupal-site-dependencies)
   then the module's dependencies will be installed automatically once the module itself is installed
   via Composer.

 * In case you manage your site dependencies manually or via Drush, install
   [Entity API](https://www.drupal.org/project/entity) and [Token](https://www.drupal.org/project/token) modules.
   Then install required libraries via
   [Composer](https://www.drupal.org/docs/8/extending-drupal/installing-modules-composer-dependencies)
   using following command:

   `composer require drupal/node_field_values:^1.0`

   You can find a bit more info about Composer [here](https://www.drupal.org/node/2804889#comment-11651131).

 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

# Usage
Inside any type of code (class, preprocess, hook, etc ..):\
$values = \Drupal::service('node_field_values.field_values')->getValues($node)\
Now you will have access to every field inside the node and every field will have it's full values already loaded, even if they are paragraphs or entity references.